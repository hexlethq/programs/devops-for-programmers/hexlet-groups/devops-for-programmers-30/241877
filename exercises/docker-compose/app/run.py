import logging
import os

import yaml
from aiohttp import web
from dotenv import load_dotenv
from sqlalchemy import create_engine


load_dotenv()
with open(os.getenv("CONFIG_PATH", "config.yml")) as stream:
    conf = yaml.safe_load(stream)


async def view_root(request):
    """Base root view to check if everythink is fine."""
    return web.Response(text="Everything is OK")


async def view_users(request):
    """Shows names and emails of users from database."""
    DB_CONFIG = conf["db"]
    URL = (
        f'postgresql://{DB_CONFIG["user"]}:{DB_CONFIG["password"]}'
        f'@{DB_CONFIG["host"]}:{DB_CONFIG["port"]}/{DB_CONFIG["database"]}'
    )
    engine = create_engine(
        URL,
        echo=False,
        isolation_level="AUTOCOMMIT",
    )
    result = []
    with engine.connect() as conn:
        users = conn.execute("SELECT * FROM users;")
        for user in users:
            result.append(dict(name=user.name, email=user.email))
    return web.json_response(data=dict(users=result))


def main():
    logging.basicConfig(**conf.get("logger"))
    logger = logging.getLogger()
    logger.info("Logging enabled")

    app = web.Application()
    app.router.add_view("/", view_root)
    app.router.add_view("/users", view_users)
    logger.info("App started")
    web.run_app(app, port=os.getenv("APP_PORT"))


if __name__ == "__main__":
    main()
