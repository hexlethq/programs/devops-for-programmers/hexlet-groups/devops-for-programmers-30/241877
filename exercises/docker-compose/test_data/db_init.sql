CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);

insert into users (id, name, email, password, created_at, updated_at)
 values (1, 'Ivanov', 'ivanov@mail.com', 'password', now(), now()),
        (2, 'Mann', 'mann@mail.com', 'password', now(), now()),
        (3, 'Ferber', 'ferber@mail.com', 'password', now(), now());
